import logging
import json
from flask import Flask, request

logging.basicConfig(format='%(asctime)s - %(message)s', level=logging.INFO)

# Publish endpoints.
# Emulate AWS Lambda gateway call to different functions.
app = Flask(__name__)

@app.route("/kongo_pptx_generator", methods = ['POST'])
def kongo():
    from lambda_functions.kongo_pptx_generator import lambda_handler
    event = json.loads("{}")
    event['body'] = json.dumps(request.get_json())
    response =  lambda_handler(event, json.loads("{}"))
    return response['body'], response['statusCode']

# Daemon controller.
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=47612, debug=False)
