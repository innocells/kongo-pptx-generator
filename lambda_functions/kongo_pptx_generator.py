"""
Lambda function to generate Kongo PPTs based on
Jira info, templates, and field maps.
"""
import os
import json
import tempfile
import urllib3
import boto3
from pptx import Presentation
from pptx.util import Inches
#from jira import JIRA

SERVER = 'uat-innocells.atlassian.net'
HTTP = urllib3.PoolManager()

def lambda_handler(event, context):
    """ Main lambda function entry point """
    issue =json.loads(event['body'])
    valid_issue = check_issue(issue)
    if valid_issue:
        template =  event["queryStringParameters"]['template']
        pptx_object = get_previous_document(template)
        if not pptx_object :
            pptx_object = get_template(template)
        fields_map = get_map(template)
        temporary_file = fill_file(pptx_object, fields_map, issue)
        filename = f"[{issue['key']}]-{template}.pptx"
        upload_to_s3(temporary_file, filename)
        return {
            'statusCode': 302,
            'body': f'https://c31790cf-3494-4967-8e39-91d8f3e612c4-innocells.s3.eu-west-1.amazonaws.com/PoC-ppt/{filename}'
        }

def check_issue(issue):
    """ Check posted issue integrity"""
    return True

def get_previous_document(template):
    """ Return if exists a previous saved document in the issue. """
    return False if 1==2 else None # Phantom code to avoid linting.

def get_template(template):
    """ Return existing template from templates repository. """
     # Creating Object
    template_path = f'./test/{template}.pptx'
    return Presentation(template_path)

def get_map(template):
    """ Return existing fields map for the requested template."""
    return json.loads(open(f'./test/{template}.map.json', encoding="utf-8").read())

def fill_file(pptx, map, issue):
    """ Fill the pptx_file with the information in the issue based on fields maping. """
    for slide_id in map['slides']:
        map_slide = map['slides'][slide_id]
        pptx_slide = pptx.slides.get(int(slide_id))
        for shape in pptx_slide.shapes:
            if shape.has_table:
                print("HOAL")
            if hasattr (shape, "text"):
                print(f'{shape.name}: {shape.text}')
        
        for placehoder in pptx_slide.placeholders:
            if placehoder.has_text_frame:
                print("HOAL")
    return ""



def lambda_handler_2(event, context):
    """ Main Lambda function entry point """
    issue_key = event["queryStringParameters"]["issueKey"]

    if issue_key[:8] != "BSDROAD-":
        response = {}
        response['errorMessages'] = []
        response['errorMessages'].append("Issue does not exist or you do not have permission to see it.")
        response['errors'] = {}
        return {
            'statusCode': 404,
            'body': json.dumps(response)
        }

    filename = f'{issue_key}.pptx'
    tmpfile  = f'{tempfile.gettempdir()}//{filename}'

    issue = get_issue(
        server=SERVER,
        access_token=os.environ['JIRA_PERSONAL_ACCESS_TOKEN'],
        issue_key=issue_key)

    ppt = generate_ppt(issue)
    ppt.save(tmpfile)
    upload_to_s3(tmpfile, filename)

    return {
        'statusCode': 302,
        'body': f'https://c31790cf-3494-4967-8e39-91d8f3e612c4-innocells.s3.eu-west-1.amazonaws.com/PoC-ppt/{filename}'
    }

def get_issue(server, access_token, issue_key):
    """ Get issue information from a Jira server """
    url = f"https://{server}/rest/api/latest/issue/{issue_key}?fields=id,summary&expand="

    headers = {
        "Accept": "application/json",
        "Content-Type": "application/json",
        "Authorization": f"Basic {access_token}"
    }

    response = HTTP.request(
        method="GET",
        url=url,
        headers=headers,
        retries = False
    )

    return json.loads(response.data)

def generate_ppt(issue):
    """ Fill information from an issue to the pptx """
    # Creating Object
    ppt = Presentation()

    # To create blank slide layout
    # We have to use 6 as an argument
    # of slide_layouts
    blank_slide_layout = ppt.slide_layouts[6]

    # Attaching slide obj to slide
    slide = ppt.slides.add_slide(blank_slide_layout)

    # For adjusting the Margins in inches
    left = top = width = height = Inches(1)

    # creating textBox
    text_box = slide.shapes.add_textbox(left, top,
                                    width, height)

    # creating textFrames
    text_frame = text_box.text_frame
    text_frame.text = f"Texto leido de JIRA ({issue['key']}):\n{issue['fields']['summary']}\n\n"

    # adding Paragraphs
    text_frame.add_paragraph()

    return ppt

def upload_to_s3(file_path, file_name):
    return
    """ Upload local file to Amazon S3 """
    session = boto3.Session(
        aws_access_key_id=os.environ['AWS_S3_USERNAME'],
        aws_secret_access_key=os.environ['AWS_S3_PASSWORD']
    )

    # Upload the file
    s3_client = session.client('s3')
    response = s3_client.upload_file(
        file_path,
        os.environ['AWS_S3_BUCKET'],
        f'PoC-ppt/{file_name}',
        ExtraArgs={'ACL': "public-read", 'ContentType': "application/vnd.openxmlformats-officedocument.presentationml.presentation"})
    return response

# Testing locally
if __name__ == '__main__':
    test_event = {}
    test_event["queryStringParameters"] = {}
    test_event["queryStringParameters"]["template"] = "Kongo-GO"
    test_event["body"] = open('./test/test-sample.json', encoding="utf-8").read()
    print(lambda_handler(test_event,{}))

if __name__ == '__main2__':
    gen = {}
    gen["slide"] = {}
    gen["slide"]["000"] = {}
    gen["slide"]["000"]["element"] = {}
    gen["slide"]["000"]["element"]["14"] = {}
    gen["slide"]["000"]["element"]["14"]["type"] = "field"
    gen["slide"]["000"]["element"]["14"]["value"] = "custom_field_14"
    gen["slide"]["001"] = {}
    gen["slide"]["001"]["element"] = {}
    gen["slide"]["001"]["element"]["17"] = {}
    gen["slide"]["001"]["element"]["17"]["type"] = "function"
    gen["slide"]["001"]["element"]["17"]["value"] = "agregation"
    gen["slide"]["002"] = {}
    print(json.dumps(gen))
